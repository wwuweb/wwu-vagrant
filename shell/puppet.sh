#!/bin/sh

mkdir -p /etc/puppet/modules
(puppet module list | grep npacker-composer) || puppet module install npacker-composer
(puppet module list | grep puppetlabs-apt) || puppet module install puppetlabs-apt
(puppet module list | grep puppetlabs-concat) || puppet module install puppetlabs-concat
(puppet module list | grep puppetlabs-nodejs) || puppet module install puppetlabs-nodejs
(puppet module list | grep puppetlabs-stdlib) || puppet module install puppetlabs-stdlib
