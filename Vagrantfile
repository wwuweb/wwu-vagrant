VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = 'wwu-vagrant'
  config.vm.box_url = 'https://www.dropbox.com/s/r9bgwzrzbcsx071/vbox-trusty64.box?dl=1'
  config.vm.box_download_checksum = '84a0b3ccac1a2a9600a835a585c1a446'
  config.vm.box_download_checksum_type = 'md5'

  config.vm.provider 'virtualbox' do |vbox|
    vbox.cpus = 1
    vbox.memory = 2048
    vbox.customize ['modifyvm', :id, '--ioapic', 'on']
  end

  config.ssh.forward_agent = true

  config.vm.network 'forwarded_port', guest: 80, host: 8080
  config.vm.network 'private_network', type: 'dhcp'

  config.vm.synced_folder 'sites/', '/vagrant/sites',
    owner: 'vagrant',
    group: 'www-data',
    mount_options: ['dmode=775,fmode=664']

  if Vagrant.has_plugin?('vagrant-librarian-puppet')
    config.librarian_puppet.puppetfile_dir = 'puppet'
    config.librarian_puppet.destructive = false
  end

  config.vm.provision 'shell', path: 'shell/puppet.sh'
  config.vm.provision 'puppet' do |puppet|
    puppet.manifests_path = 'puppet/manifests'
    puppet.module_path = 'puppet/modules'
    puppet.manifest_file  = 'default.pp'
    puppet.hiera_config_path = 'puppet/hiera/hiera.yaml'
  end
end
