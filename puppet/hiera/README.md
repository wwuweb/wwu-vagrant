# README #
The wwu-hiera repository is used in conjunction with the [wwu-puppet](https://bitbucket.org/wwuweb/wwu-puppet) repository. To update an existing vagrant box with the latest hiera updates for the first time you need to do the following:

* In your vagrant directory navigate into /puppet
* Remove the existing hiera directory 
```
#!bash

rm -r hiera
```
* Next clone wwu-hiera to the hiera directory

```
#!bash

git clone git@bitbucket.org:wwuweb/wwu-hiera.git hiera
```

* Then boot your machine with vagrant up and once it is up run vagrant provision to apply the changes.

In the future you simply need to navigate into the hiera directory and run a git pull origin to retrieve the updates. Once updated run vagrant provision to apply the changes.