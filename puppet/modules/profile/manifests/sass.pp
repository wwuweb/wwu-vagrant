class profile::sass {
  $compass_version   = hiera('profile::sass::compass_version')
  $sass_version      = hiera('profile::sass::sass_version')
  $zen_grids_version = hiera('profile::sass::zen_grids_version')

  package { 'ruby1.9.1-dev':
    ensure => present
  }

  package { 'bundler':
    ensure   => present,
    provider => 'gem'
  }

  package { 'compass':
    ensure   => $compass_version,
    provider => 'gem'
  }

  package { 'sass':
    ensure   => $sass_version,
    provider => 'gem'
  }

  package { 'zen-grids':
    ensure   => $zen_grids_version,
    provider => 'gem'
  }
}
