class profile::nodejs {
  $manage_package_repo       = hiera('profile::nodejs::manage_package_repo')
  $nodejs_dev_package_ensure = hiera('profile::nodejs::nodejs_dev_package_ensure')
  $npm_package_ensure        = hiera('profile::nodejs::npm_package_ensure')

  class { '::nodejs':
    manage_package_repo       => $manage_package_repo,
    nodejs_dev_package_ensure => $nodejs_dev_package_ensure,
    npm_package_ensure        => $npm_package_ensure
  }

  package { 'nodejs-legacy':
    ensure => present
  }

  package { 'grunt-cli':
    ensure   => present,
    provider => 'npm'
  }
}
