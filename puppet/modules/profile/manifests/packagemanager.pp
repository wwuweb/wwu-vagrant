class profile::packagemanager {
  $provider = hiera('profile::packagemanager::updater::provider')

  updater::instance { $provider: }
}
