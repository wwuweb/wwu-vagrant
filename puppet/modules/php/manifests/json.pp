class php::json {
  package{ 'php5-json':
    ensure  => present,
    require => Package['php']
  }
}