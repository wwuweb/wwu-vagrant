class php::mysql {
  package{ 'php5-mysql':
    ensure  => present,
    require => Package['php']
  }
}