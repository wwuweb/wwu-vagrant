class php::mcrypt {
  package{ 'php5-mcrypt':
    ensure  => present,
    require => Package['php']
  }

  file { '/etc/php5/apache2/conf.d/mcrypt.ini':
    ensure  => link,
    target  => '/etc/php5/mods-available/mcrypt.ini',
    owner   => 'root',
    group   => 'root',
    notify  => Service['apache'],
    require => Package['php5-mcrypt']
  }
}