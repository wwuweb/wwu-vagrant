class php::xdebug {
  package{ 'php5-xdebug':
    ensure  => present,
    require => Package['php']
  }

  $path = '/etc/php5/mods-available/xdebug.ini'

  file_line { 'xdebug-remote-enable':
    path    => $path,
    line    => 'xdebug.remote_enable=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-extended-info':
    path    => $path,
    line    => 'xdebug.extended_info=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-remote-port':
    path    => $path,
    line    => 'xdebug.remote_port=9000',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-remote-connect-back':
    path    => $path,
    line    => 'xdebug.remote_connect_back=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-var-display-max-depth':
    path    => $path,
    line    => 'xdebug.var_display_max_depth=5',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-max-nesting-level':
     path    => $path,
     line    => 'xdebug.max_nesting_level=256',
     notify  => Service['apache'],
     require => Package['php5-xdebug']
   }
}
