class cachefilesd (
  $install = true
) {
  if $install == false {
    $ensure = absent
  } else {
    $ensure = present
  }

  package { 'cachefilesd':
    ensure => present
  }

  service { 'cachefilesd':
    ensure  => running,
    require => Package['cachefilesd']
  }

  file_line { 'run_cachefilesd':
    path    => '/etc/default/cachefilesd',
    line    => 'RUN=yes',
    require => Package['cachefilesd']
  }

  file_line { 'bcull_cachefiledsd':
    path    => '/etc/default/cachefilesd',
    line    => 'bcull 50%',
    match   => 'bcull',
    require => Package['cachefilesd']
  }
}
