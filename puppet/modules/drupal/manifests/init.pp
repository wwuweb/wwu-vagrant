class drupal (
  $runas       = $drupal::params::runas,
  $doc_root    = $drupal::params::doc_root,
  $db_driver   = $drupal::params::db_driver,
  $db_hostname = $drupal::params::db_hostname,
  $db_port     = $drupal::params::db_port,
  $db_name     = undef,
  $db_user     = undef,
  $db_password = undef
) inherits drupal::params {}