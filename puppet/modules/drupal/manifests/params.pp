class drupal::params {
  $runas       = 'vagrant'
  $doc_root    = '/var/www'
  $db_driver   = 'mysql'
  $db_hostname = 'localhost'
  $db_port     = '3306'
}
