define mysql::database (
  $ensure = present,
  $database = $title
) {
  if ! defined(Class['mysql']) {
    fail('Class mysql must be defined.')
  }

  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")

  if $ensure == 'present' {
    mysql::query { "${database}-database-present":
      query => template('mysql/create-database.erb')
    }
  } elsif $ensure == 'absent' {
    mysql::query { "${database}-database-absent":
      query => template('mysql/drop-database.erb')
    }
  }
}