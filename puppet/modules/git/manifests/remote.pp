define git::remote (
  $ensure     = present,
  $host       = undef,
  $protocol   = 'ssh'
) {
  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")
  validate_re($protocol, '^(ssh|https)$',
  "${protocol} is not supported for protocol. Allowed values are 'ssh' or 'http'")
  validate_re($title, '^.+:.+$',
  "${title} is not supported for remote name. Allowed values follow the format repository:remote")

  $parts = split($title, ':')
  $repository = $parts[0]
  $remote = $parts[1]

  if ! defined(Git::Repository[$repository]) {
    fail("Git repository ${repository} is not defined.")
  }

  $repository_ensure = getparam(Git::Repository[$repository], 'ensure')
  $repository_directory = getparam(Git::Repository[$repository], 'directory')
  $repository_owner = getparam(Git::Repository[$repository], 'owner')

  Exec {
    user => $repository_owner,
    cwd  => $repository_directory,
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $repository_ensure == 'present' {
    if $ensure == 'present' {
      $url = $protocol ? {
        'https' => "https://${host}/${repository}.git",
        default => "git@${host}:${repository}.git"
      }

      exec { "git-remote-add-${repository}:${remote}":
        command => "git remote add ${remote} ${url}",
        unless  => "git remote --verbose | grep '${remote}' >/dev/null 2>/dev/null",
        require => [
          Exec["git-init-${repository}"],
          Exec["git-clone-${repository}"]
        ]
      }

      exec { "git-remote-update-${repository}:${remote}":
        command => "git remote set-url ${remote} ${url}",
        unless  => "git remote --verbose | grep '${url}' >/dev/null 2>/dev/null",
        require => Exec["git-remote-add-${repository}:${remote}"]
      }
    } else {
      exec { "git-remote-remove-${repository}:${remote}":
        command => "git remote remove ${remote}",
        onlyif  => "git remote | grep '${remote}' >/dev/null 2>/dev/null",
        require => [
          Exec["git-init-${repository}"],
          Exec["git-clone-${repository}"]
        ]
      }
    }
  }
}
